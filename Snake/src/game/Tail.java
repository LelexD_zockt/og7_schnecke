package game;

public class Tail {
	int x, y;
	boolean wait = true;

	public Tail(int x, int y) {
		super();
		this.x = x;
		this.y = y;

	}



	public int getX() {
		return x;
	}

	void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	void setY(int y) {
		this.y = y;
	}

	public boolean isWait() {
		return wait;
	}

	void setWait(boolean wait) {
		this.wait = wait;
	}
}
