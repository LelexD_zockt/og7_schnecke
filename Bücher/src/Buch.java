
public class Buch implements Comparable<Buch> {
	private String autor;
	private String titel;
	private String isbn;

	public Buch(String autor, String titel, String isbn) {
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Autor: " + this.getAutor() + "Titel: " + this.getTitel() + "ISBN: " + this.getIsbn();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Buch))
			return false;
		return this.getIsbn().equals(((Buch) o).getIsbn());
	}

	@Override
	public int compareTo(Buch arg0) {

		return this.getIsbn().compareTo(arg0.getIsbn());
	}

}
