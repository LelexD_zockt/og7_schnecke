import java.util.*;

public class CollectionTest {
	static List<Buch> buchliste = new LinkedList<Buch>();
	static Scanner myScanner = new Scanner(System.in);

	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) loeschen");
		System.out.println(" 4) Die groesste ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				buchliste.add(CollectionTest.Bucheintrag());
				System.out.println("\nWurde eingetragen");
				break;
			case '2':
				String isbn = myScanner.next();
				CollectionTest.findeBuch(buchliste, isbn).toString();
				System.out.println("\n BOOM Gefunden!:");
				break;
			case '3':
				CollectionTest.loescheBuch();
				System.out.println("\nWurde gel�scht");
				break;
			case '4':
				CollectionTest.ermitteleGroessteISBN(buchliste);
				System.out.println("\nDie gr��e der ISBN-Nummer ist: ");
				break;
			case '5':
				System.out.println("zeigen: ");
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	public static Buch Bucheintrag() {
		System.out.println("Titel eingeben: ");
		String titel = myScanner.next();
		System.out.println("Autor eingeben: ");
		String autor = myScanner.next();
		System.out.println("ISBN-Nummer eingeben: ");
		String ISBN = myScanner.next();
		return new Buch(titel, autor, ISBN);
	}

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) 
			if (buchliste.get(i).getIsbn().equals(isbn))
				return buchliste.get(i);
			return null;
		
	}

	public static boolean loescheBuch() {
		int a;
		System.out.print("Index: ");
		a = myScanner.nextInt();
		if (a < buchliste.size() && a > -1) {
			buchliste.remove(a);
			return true;
		}
		return false;
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		Buch temp = buchliste.get(0);
		for (int i = 0; i < buchliste.size(); i++) {
			if (temp.compareTo(buchliste.get(i)) < 0) {
				temp = buchliste.get(i);
			}

		}
		return temp.getIsbn();

	}

}
