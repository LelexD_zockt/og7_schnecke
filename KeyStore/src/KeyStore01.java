public class KeyStore01 {

	
	private String[] keystore;

	public KeyStore01() {
		this.keystore = new String[100];
	}

	
	public KeyStore01(int lenght) {
		keystore = new String[lenght];
	}

	
	public String get(int i) {
		return this.keystore[i];
	}

	public String[] size() {

		return this.keystore;
	}

	
	public void clear() {
		this.keystore = 0;
	}

	
	public boolean add(String e) {
		for(int i = 0; i < keystore.length; i++)
		if (this.keystore[i] == null) {
			this.keystore[i] = e;
			return true;
		}
		return false;
	}

	
	public boolean remove(int i) {
		if ((i >= 0) && (i < this.keystore.length)) {
			for (int x = i; x < keystore.length - 1; x++) {	
				keystore[x] = this.keystore[x + 1];
			}
			
			return true;
		}
		return false;
	}

	
	public boolean remove(String wort) {
		if (indexOf(wort) == -1)
			return false;
		return remove(indexOf(wort));
	}


	public int indexOf(String eintrag) {
		for(int i = 0; i < this.keystore.length; i++) {
			if(this.keystore[i].equals(eintrag))
					return i; 
		}
	}
	
	//@override
	public String toString() {
		String s = "";
		for (int i = 0; i < this.keystore.length; i++)
			s+= this.keystore[i]+  "\n";
		return s;
	}

}