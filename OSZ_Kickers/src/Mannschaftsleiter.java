
public class Mannschaftsleiter extends Player {
	private String mannschaftsname;
	private int rabatt;
	
	public Mannschaftsleiter() {
		mannschaftsname = "OSZ Kickers";
		rabatt = 50;
		
	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	public void organisiere() {
		
	}

}
