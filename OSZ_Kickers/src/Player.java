
public class Player extends Person {
	private int trikotNr;
	private String spielPos;
	
	public Player() {
		trikotNr = 1;
		spielPos = "Tor";
		
	}

	public int getTrikotNr() {
		return trikotNr;
	}

	public void setTrikotNr(int trikotNr) {
		this.trikotNr = trikotNr;
	}

	public String getSpielPos() {
		return spielPos;
	}

	public void setSpielPos(String spielPos) {
		this.spielPos = spielPos;
	}

}
