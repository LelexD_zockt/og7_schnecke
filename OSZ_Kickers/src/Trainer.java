
public class Trainer extends Person {
	private char lizenzKlasse;
	private int monAufSchaedigung;
	
	public Trainer() {
		lizenzKlasse = 'a';
		monAufSchaedigung = 210;
		
	}

	public String getLizenzKlasse() {
		return lizenzKlasse;
	}

	public void setLizenzKlasse(String lizenzKlasse) {
		this.lizenzKlasse = lizenzKlasse;
	}

	public int getMonAufSchaedigung() {
		return monAufSchaedigung;
	}

	public void setMonAufSchaedigung(int monAufSchaedigung) {
		this.monAufSchaedigung = monAufSchaedigung;
	} 

}
