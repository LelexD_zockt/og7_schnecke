
public class Schiedsrichter extends Person {
	private int anzPfiffSpiele;
	
	public Schiedsrichter() {
		anzPfiffSpiele = 5;
		
	}

	public int getAnzPfiffSpiele() {
		return anzPfiffSpiele;
	}

	public void setAnzPfiffSpiele(int anzPfiffSpiele) {
		this.anzPfiffSpiele = anzPfiffSpiele;
	}

}
