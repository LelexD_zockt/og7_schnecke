package karte;

public class Held {
	private String name;
	private String typ;
	private String beschreibung;
	private int maxLeben;
	private int angreifen;
	private int ruestung;
	private int magieresistenz;
	private int aktLeben;
	private String bildpfad;
	
	public Held() {
		this.name = "";
		this.typ = "";
		this.beschreibung = "";
		this.bildpfad = null;
		this.maxLeben = 0;
		this.angreifen = 0;
		this.ruestung= 0;
		this.magieresistenz = 0;
		this.aktLeben= 0;
	}

	public Held(String name, String typ, String beschreibung, int maxLeben, int ruestung, int magieresistenz, int angriff
		) {
		super();
		this.name = name;
		this.typ = typ;
		this.beschreibung = beschreibung;
		this.maxLeben = maxLeben;
		this.angreifen = angriff;
		this.ruestung = ruestung;
		this.magieresistenz = magieresistenz;
		this.aktLeben = maxLeben;
		this.bildpfad = ".src/bilder/" + name + ".jpeg";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getMaxLeben() {
		return maxLeben;
	}

	public void setMaxLeben(int maxLeben) {
		this.maxLeben = maxLeben;
	}

	public int getAngreifen() {
		return angreifen;
	}

	public void setAngreifen(int angreifen) {
		this.angreifen = angreifen;
	}

	public int getRuestung() {
		return ruestung;
	}

	public void setRuestung(int ruestung) {
		this.ruestung = ruestung;
	}

	public int getMagieresistenz() {
		return magieresistenz;
	}

	public void setMagieresistenz(int magieresistenz) {
		this.magieresistenz = magieresistenz;
	}

	public int getAktLeben() {
		return aktLeben;
	}

	public void setAktLeben(int aktLeben) {
		this.aktLeben = aktLeben;
	}
	
	public void leiden(int schaden) {
		if(schaden - this.ruestung <=0)
			aktLeben -= 1;
		else aktLeben -= schaden - ruestung;
	}
	
	public void heilen() {
		this.aktLeben = this.maxLeben;
	}
	
	public String getBildpfad() {
		return bildpfad;
	}

	public void setBildpfad(String bildpfad) {
		this.bildpfad = bildpfad;
	}

	public int angreifen() {
		return this.angreifen;
	}

	@Override
	public String toString() {
		return "Held [name=" + name + ", typ=" + typ + ", beschreibung=" + beschreibung + ", maxLeben=" + maxLeben
				+ ", angreifen=" + angreifen + ", ruestung=" + ruestung + ", magieresistenz=" + magieresistenz
				+ ", aktLeben=" + aktLeben + "]";
	}
	
	

}
