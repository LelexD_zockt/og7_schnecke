package karte;

import javax.swing.JPanel;

public class Karte extends JPanel {
	private Held held;

	public Karte(Held held) {
		super();
		this.held = held;
	}

	public Held getHeld() {
		return held;
	}

	public void setHeld(Held held) {
		this.held = held;
	}
	
	
}
