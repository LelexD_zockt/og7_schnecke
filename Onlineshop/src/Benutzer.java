
public class Benutzer {
	String benutzerID; 
	String password;
	String name;
	String email;
	
	public void setBenutzerID(String benutzerID) {
		this.benutzerID = benutzerID;
	}
	
	public String getBenutzerID() {
		return benutzerID;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void einloggen() {
		this.einloggen();
	}

}
