
public class Lerneintrag {
	String fach;
	String beschreibung;
	int dauer;
	String meinString = "16.01.2017";
	

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	@Override
	public String toString() {
		return "Lerneintrag [fach=" + fach + ", beschreibung=" + beschreibung + ", dauer=" + dauer + "]";
	}

}
