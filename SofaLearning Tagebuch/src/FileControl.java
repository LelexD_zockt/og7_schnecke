import java.io.*;
import java.io.FileReader;

public class FileControl {
	public void dateiEinlesenUndAusgeben(File file) {
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String s = br.readLine();
			while (s != null) {
				System.out.println("Datum   /    Fach   /              Aktivitšt          / Dauer");
				System.out.println("----------------------------------------------------------------");
				System.out.println(s + "/");
				s = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		File file = new File("miriam.dat");
		FileControl filecontrol = new FileControl();
		filecontrol.dateiEinlesenUndAusgeben(file);

	}

}
