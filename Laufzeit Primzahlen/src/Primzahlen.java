
public class Primzahlen {

	public static void main(String[] args) {
		long zahl = 997; // Zahl kann hier ge�ndert werden
		if (Primzahlen.prim(zahl))
			System.out.print(zahl + "ist eine Primzahl");
		else
			System.out.print(zahl + "ist keine Primzahl");

	}

	public static boolean prim(long zahl) {
		if (2 > zahl)
			return false;
		for (long i = 2; i < zahl; i++) {
			if (zahl % i == 0)
				return false;
		}
		return true;

	}
}
