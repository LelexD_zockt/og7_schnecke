
public class Stoppuhr {
	private static long millis;

	public static void start() {
		millis = System.currentTimeMillis();
	}

	public static long stop() {
		return System.currentTimeMillis() - millis;

	}

}
