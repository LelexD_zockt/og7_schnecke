package test;

import java.util.LinkedList;
import java.util.Random;
import java.awt.Color;
import java.awt.Graphics;

import controller.BunteRechteckeController;
import src.model.Rechteck;
import src.model.override;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck rechteck0 = new Rechteck(10, 10, 30, 40);
		Rechteck rechteck1 = new Rechteck(25, 25, 100, 20);
		Rechteck rechteck2 = new Rechteck(260, 10, 200, 100);
		Rechteck rechteck3 = new Rechteck(5, 500, 300, 25);
		Rechteck rechteck4 = new Rechteck(100, 100, 100, 100);
		Rechteck rechteck5 = new Rechteck();
		Rechteck rechteck6 = new Rechteck();
		Rechteck rechteck7 = new Rechteck();
		Rechteck rechteck8 = new Rechteck();
		Rechteck rechteck9 = new Rechteck();
		Rechteck eck10 = new Rechteck(-4, -5, -50, -200);
		System.out.println(eck10); // Rechteck[x=-4, y=-5, breite=50, hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11); // Rechteck[x=-10, y=-10, breite=-200, hoehe=-100]

		rechteck5.setBreite(200);
		rechteck5.setHoehe(200);
		rechteck5.setX(200);
		rechteck5.setY(200);

		rechteck6.setBreite(200);
		rechteck6.setHoehe(200);
		rechteck6.setX(200);
		rechteck6.setY(200);

		rechteck7.setBreite(200);
		rechteck7.setHoehe(200);
		rechteck7.setX(200);
		rechteck7.setY(200);

		rechteck8.setBreite(200);
		rechteck8.setHoehe(200);
		rechteck8.setX(200);
		rechteck8.setY(200);

		rechteck9.setBreite(200);
		rechteck9.setHoehe(200);
		rechteck9.setX(200);
		rechteck9.setY(200);

		if (rechteck0.toString().equals("Rechteck: [x=10, y=10, breite:30, hoehe=40]")) {
			System.out.println("Khajiit has wares if you have coin");
		}
		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		System.out.println(controller.toString());

	}

	public static void rechteckeTesten() {
		Rechteck[] rechtecke = new Rechteck[50000];
		for (int i = 0; i < rechtecke.length; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
			System.out.println(rechtecke[i].toString());
		}

	}
}
