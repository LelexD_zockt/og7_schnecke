package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import src.model.Rechteck;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SchönesFenster extends JFrame {
	private BunteRechteckeController ctr;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SchönesFenster frame = new SchönesFenster(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param bunteRechteckeController
	 */
	public SchönesFenster(BunteRechteckeController bunteRechteckeController) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		this.ctr = bunteRechteckeController;

		JLabel lblNewLabel_3 = new JLabel("X:");
		contentPane.add(lblNewLabel_3);

		textField_3 = new JTextField();
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Y:");
		contentPane.add(lblNewLabel_2);

		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Hoehe:");
		contentPane.add(lblNewLabel_1);

		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblNewLabel = new JLabel("Breite:");
		contentPane.add(lblNewLabel);

		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("speichern");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				neuesRechteck();
			}
		});
		btnNewButton.setForeground(Color.DARK_GRAY);
		setVisible(true);
		contentPane.add(btnNewButton);
	}

	protected void neuesRechteck() {
		ctr.add(new Rechteck(Integer.parseInt(textField_3.getText()), Integer.parseInt(textField_2.getText()),
				Integer.parseInt(textField_1.getText()), Integer.parseInt(textField.getText())));

	}

}
