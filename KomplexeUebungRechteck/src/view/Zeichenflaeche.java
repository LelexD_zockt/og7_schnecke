package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import controller.BunteRechteckeController;
import src.model.override;

public class Zeichenflaeche extends JPanel {

	private final BunteRechteckeController kontrolle;

	public Zeichenflaeche(BunteRechteckeController kontrolle) {
		super();
		this.kontrolle = kontrolle;
	}

	@override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);

		for (int i = 0; this.kontrolle.getRechtecke().size() > i; i++) {
			g.drawRect(this.kontrolle.getRechtecke().get(i).getY(), this.kontrolle.getRechtecke().get(i).getX(),
					this.kontrolle.getRechtecke().get(i).getBreite(), this.kontrolle.getRechtecke().get(i).getHoehe());
		}

	}
}
