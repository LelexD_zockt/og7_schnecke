package controller;

import java.util.LinkedList;

import src.model.MySQLDatabase;
import src.model.Rechteck;
import view.SchönesFenster;

public class BunteRechteckeController {

	private LinkedList<Rechteck> rechtecke;
	private MySQLDatabase database;

	public static void main(String[] args) {

	}

	public BunteRechteckeController() {
		this.rechtecke = new LinkedList<Rechteck>();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}

	public BunteRechteckeController(LinkedList<Rechteck> rechtecke) {
		super();
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		this.rechtecke.add(rechteck);
		this.database.rechteckEintragen(rechteck);
	}

	public void reset() {
		this.rechtecke.clear();

	}

	@Override
	public String toString() {
		String ausgabe = "BunteRechteckeController [rechtecke=";
		for (int i = 0; this.rechtecke.size() > i; i++) {
			ausgabe += " [" + this.rechtecke.get(i).toString();
			if (i != this.rechtecke.size() - 1) {
				ausgabe += ",";
			}
		}
		ausgabe += "]";
		return ausgabe;
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	private void setRechtecke(LinkedList<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void generiereZufallsRechtecke(int anzahl) {
		rechtecke.clear();
		for (int i = 0; i < anzahl; i++) {
			rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}

	public void rechteckhinzufuegen() {
		new SchönesFenster(this);
		
	}

}
