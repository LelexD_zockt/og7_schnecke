package src.model;

public class Rechteck {
	int x;
	int y;
	int hoehe;
	int breite;

	public Rechteck() {
		x = 0;
		y = 0;
		hoehe = 0;
		breite = 0;

	}

	public Rechteck(int x, int y, int hoehe, int breite) {
		super();
		this.x = Math.abs(x);
		this.y = Math.abs(y);
		this.hoehe = Math.abs(hoehe);
		this.breite = Math.abs(breite);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = Math.abs(x);
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = Math.abs(y);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	@override // ‹berschreibt die toString-Methode, welche von jedem Objekt geerbt wird.
	public String toString() {
		return "Rechteck: [x=" + this.x + ", y=" + this.y + ", breite=" + this.breite + ", hoehe=" + this.hoehe + "]";
	}

	public boolean enthaelt(int x, int y) {
		if (this.y <= y && this.x <= x) {
			if (this.y + this.hoehe >= y && this.x + this.breite >= x) {
				return true;
			}
		}
		return false;
	}

	public boolean enthaelt(Punkt p) {
		if (enthaelt(p.getX(), p.getY())) {
			return true;
		}
		return false;

	}
//<<<<<<< HEAD

	public static Rechteck generiereZufallsRechteck() {
		int hoehe = ((int) Math.random() * 1200);
		int breite = ((int) Math.random() * 1200);
		int y = ((int) Math.random() * 1000 - hoehe);
		int x = ((int) Math.random() * 1000 - breite);
		return new Rechteck(x, y, hoehe, breite);
	}

//=======

	public boolean enthaelt(Rechteck rechteck) {
		rechteck.enthaelt(this.x, this.y);
		return false;
	}

}
//>>>>>>> branch 'master' of https://LelexD_zockt@bitbucket.org/LelexD_zockt/og7_schnecke.git
