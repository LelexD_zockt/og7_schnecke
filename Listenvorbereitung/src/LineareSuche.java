
public class LineareSuche {
	public static int lineareSuche(int gesucht, int array[]) {

		int i = 0;

		while (i < array.length) {
			if (array[i] == gesucht) {
				return i;

			}
			i++;

		}
		return -1;
	}
}
